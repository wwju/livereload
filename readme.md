# liveReload - библиотека для автоперезагрузки страниц

Я - еще один чувак, которому надоело жать Ctrl+S Alt+Tab Ctrl+R, чтобы увидеть результаты каждого сохранения файла, будь это HMTL и CSS, или более продвинутые HAML, JavaScript и SCSS.

"Фигня вопрос!" - сказал я, и за выходные написал первую версию кроссбраузерного перезагрузчика страниц.

Теперь я могу жать Ctrl+S Alt+Tab, а страницы будут перезагружаться автоматически :) Чему я несказанно счастлив!

##  Что это делает?

* Вы редактируете CSS-файл ( Можно вообще любой, хоть *.foobar )
* Сохраняете
* Все страницы, над которыми вы вели работу, перезагрузились

## Я тоже так хочу!

Установите liveReload

<pre>
	npm install livereload
</pre>

В текстовый редактор VIM добавьте строчки:

( ведь вы же сохраняете не при помощи *:w*, а одним сочетанием клавиш!? )

В редактируемую html-страницу добавьте скрипты

<pre>
	http://localhost:8089/request.js
	http://localhost:8089/livereload.js
</pre>

В консоли:

<pre>
	livereload
</pre>

Пересохраните файл в текстовом редакторе

О чудо! Страницы в браузере перезагрузились!

## Мааленькая документация.

Как это работает...

Указанные в файле ниже 
Вы жмете на Ctrl+S, редактор сохраняет документ



## Что не работает, или TODO

* Пока что работает только в VIM. Но эту возможность легко добавить и в другие текстовые редакторы
* Если закрыть сервер liveReload, то скрипт в браузере не прекратит обращаться к серверу и будет засорять консоль сообщениями об ошибке
* Добавить поддержку настроек
* Добавить веб-страницу, в которой будут редактироваться настройки
* Добавить на каждую страницу кнопку, которая останавливает действие скрипта
* Добавить хоть какую-то
* Если в инспекторе объектов произошли изменения, страница перезагрузится
* Реализовать запросы при помощи WebSockets
* Нет тестов
* Перезагружается вся страница. Было бы неплохо перезагружать только измененные файлы
* Записать видео работы библиотеки
* Перевести readme.md на английский
* В пункт *Как это работает* нарисовать схему
