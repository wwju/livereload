window.liveReload = {}

liveReload.update_date = ( date ) ->
  liveReload.url = 'http://localhost:8089/need_reload?date=' +
    date

get_time = new Date().getTime()

liveReload.update_date( get_time )

liveReload.request_need_reload = ->
  reqwest liveReload.request_object

liveReload.start = ->
  liveReload.stop_id = setInterval liveReload.request_need_reload, 500
  return

liveReload.stop = ->
  clearInterval liveReload.stop_id

liveReload.new_request_object = ->
  liveReload.request_object = 
    url: liveReload.url
    type: 'GET'
    crossOrigin: true
    success: liveReload.success_function
  return

liveReload.success_function = ( data ) ->
  date = data.response
  if date is "false"
    return
  else if date.request is "true"
    liveReload.stop
    return
  else
    liveReload.url = liveReload.update_date parseInt( date )
    liveReload.new_request_object()
    window.location.reload()
    return

liveReload.new_request_object()

liveReload.start()
